﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

	public float maxRayDistance = 30.0f;
	public LayerMask collisionLayer = 1 << 10;  //ARKitPlane layer

	public Vector3 planePosition = new Vector3();
	public Quaternion planeRotation = new Quaternion();
	public Text scoreText;
	public Text playerHealthText;
	public Text gameOverText;
	public int maxHealth = 100;

	private bool hasPlaneBeenFound = false;
	private int score = 0;
	private int playerHealth = 0;

	void Start()
	{
		SetScore(0);
		SetPlayerHealth(maxHealth);
		gameOverText.text = "";
	}

	void Update()
	{
		if (!hasPlaneBeenFound)
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit, maxRayDistance, collisionLayer))
			{
				hasPlaneBeenFound = true;

				planePosition = hit.point;
				planeRotation = hit.transform.rotation;

				GameObject spawnPointGameObject = GameObject.FindGameObjectWithTag("SpawnPoint");
				SpawnPoint spawnPoint = spawnPointGameObject.GetComponent<SpawnPoint>();
				spawnPoint.StartSpawn(hit.point, hit.transform.rotation);

			}
		}
	}

	void SetScore(int score)
	{
		this.score = score;
		scoreText.text = "Score: " + score.ToString();
	}

	void SetPlayerHealth(int health)
	{
		this.playerHealth = health;
		playerHealthText.text = "Health: " + playerHealth.ToString();
	}

	public void AddToScore(int amount)
	{
		SetScore(score + amount);
	}

	public void SubtractFromPlayerHealth(int amount)
	{
		SetPlayerHealth(playerHealth - amount);
		if (playerHealth <= 0)
		{
			gameOverText.text = "GAME OVER";
		}
	}

}
