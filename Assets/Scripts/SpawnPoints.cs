﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoints : MonoBehaviour {

	public GameObject enemyPrefab;

	private bool spawned = false;

	void Start() {
	}

	public void PlaneTransformUpdated(Transform planeTransform)
	{
		SpawnEnemies();
	}

	void SpawnEnemies() {
		if (!spawned) {
			spawned = true;
			// find all spawn points
			GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag("Spawn");
			// spawn an enemy at the points
			foreach (GameObject spawnPoint in spawnPoints)
			{
				Instantiate(enemyPrefab, spawnPoint.transform);
			}
		}
	}
	
}
