﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	public float lifetime = 3.0f;

	private int damage = 1;

	void Start()
	{
		Destroy(gameObject, lifetime);
	}

	void Update()
	{
	}

	public void SetDamage(int damage)
	{
		this.damage = damage;
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Enemy")
		{
			Enemy enemy = other.gameObject.GetComponent<Enemy>();
			if (enemy.SubtractFromHealth(damage))
			{
				GameObject gameManagerObject = GameObject.FindGameObjectWithTag("GameManager");
				GameManager gameManager = gameManagerObject.GetComponent<GameManager>();
				gameManager.AddToScore(3);
			}
			Destroy(gameObject);
		}
	}

}
