﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

	public GameObject projectilePrefab;
	public GameObject fireballPrefab;
	public float projectileSpeed = 100.0f;
	public int projectileDamage = 2;
	public int fireballDamage = 5;

	private bool planeInitialized = false;
	private bool projectilesEnabled = false;
	private bool fireballEnabled = true;
	private Transform planeTransform;

	void Start()
	{
	}

	void Update()
	{
		if (planeInitialized)
		{
			Vector3 position = Camera.main.transform.position;
			position.y = planeTransform.position.y;
			transform.position = position;
		}
	}

	void LaunchProjectile(GameObject prefab, int damage)
	{
		if (projectilesEnabled)
		{
			GameObject projectile = Instantiate(prefab, Camera.main.transform);
			Rigidbody rigidBody = projectile.GetComponent<Rigidbody>();
			rigidBody.AddForce(transform.forward * projectileSpeed);
			projectile.transform.parent = null; // this is so projectile doesn't move with camera
			projectile.GetComponent<Projectile>().SetDamage(damage);
		}
	}

	public void PlaneTransformUpdated(Transform planeTransform)
	{
		this.planeTransform = planeTransform;
		planeInitialized = true;
		StartCoroutine("EnableProjectiles");
	}

	// wait for a second after selecting plane to allow shooting
	IEnumerator EnableProjectiles()
	{
		yield return new WaitForSeconds(1.0f);
		projectilesEnabled = true;
	}

	IEnumerator FireballCooldown()
	{
		fireballEnabled = false;
		// disable button somehow?
		yield return new WaitForSeconds(5.0f);
		// enable button somehow?
		fireballEnabled = true;
	}

	public void ShootClicked()
	{
		LaunchProjectile(projectilePrefab, projectileDamage);
	}

	public void FireballClicked()
	{
		if (fireballEnabled)
		{
			LaunchProjectile(fireballPrefab, fireballDamage);
			StartCoroutine("FireballCooldown");
		}
	}

}
