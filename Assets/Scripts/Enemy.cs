﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Enemy : MonoBehaviour
{

	enum States
	{
		LOOKING = 0,
		WANDERING,
		MOVE_TO_PLAYER}

	;

	public float rotationChangeSeconds = 3.0f;
	public float rotationSpeed = 1.0f;
	public float sightDistance = 10.0f;
	public float movementSpeed = 0.05f;
	public float attentionSpan = 5.0f;
	public float idleTime = 15.0f;
	public float wanderRadius = 15.0f;
	public int damage = 10;
	public int health = 10; 

	private States state = States.LOOKING;
	private Quaternion rotation;
	private float lastTimeStateChanged;
	private Vector3 wanderPosition;
	private GameObject player;

	void Start()
	{
		StartCoroutine("PickRotation");
		player = GameObject.FindGameObjectWithTag("Player");
		Rigidbody rigidBody = GetComponent<Rigidbody>();
		rigidBody.WakeUp();

	}

	void Update()
	{
		switch (state) {
		case States.LOOKING:
			UpdateLooking();
			break;

		case States.WANDERING:
			UpdateWandering();
			break;

		case States.MOVE_TO_PLAYER:
			UpdateMoveToPlayer();
			break;
		}
		Look();
	}

	void UpdateLooking()
	{
		// scan the room to find player
		transform.rotation = Quaternion.Lerp(transform.rotation, rotation, rotationSpeed * Time.deltaTime);
		// after 15 seconds, switch to wander state
		if (Time.time - lastTimeStateChanged > idleTime) {
			SetState(States.WANDERING);
		}
	}

	void UpdateWandering()
	{
		// if destination is reached, pick a new destination
		float distance = Vector3.Distance(transform.position, wanderPosition);
		//Debug.Log(distance);
		if (distance < 1.0f) {
			PickWanderingPosition();
		}

		// move to new position
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(wanderPosition - transform.position), rotationSpeed * Time.deltaTime);
		transform.position += transform.forward * Time.deltaTime * movementSpeed;

		// after 15 seconds, switch to looking state
		if (Time.time - lastTimeStateChanged > idleTime) {
			SetState(States.LOOKING);
		}
	}

	void UpdateMoveToPlayer()
	{
		// move in the direction of the player
		float distance = Vector3.Distance(transform.position, player.transform.position);
		if (distance < 1.0f)
		{
			HitPlayer();
		} else 
		{
			transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(player.transform.position - transform.position), rotationSpeed * Time.deltaTime);
			transform.position += transform.forward * Time.deltaTime * movementSpeed;
		}
	}

	void Look()
	{
		Debug.DrawRay(transform.position, transform.forward * sightDistance, Color.green);
		// cast ray looking for player
		RaycastHit hit;
		if (Physics.Raycast(transform.position, transform.forward, out hit, sightDistance)) {
			// if player is seen, switch to movetoplayer state
			if (hit.collider.tag == "Player") {
				SetState(States.MOVE_TO_PLAYER);
			}
		} else {
			if (state == States.MOVE_TO_PLAYER) {
				// if player is not seen for 5 seconds, switch to looking state
				if (Time.time - lastTimeStateChanged > attentionSpan) {
					Debug.Log("LOST PLAYER");
					SetState(States.LOOKING);
				}
			}
		}
	}

	void PickWanderingPosition()
	{
		wanderPosition = Random.insideUnitSphere * wanderRadius;
		wanderPosition.y = transform.position.y;
	}

	void SetState(States newState)
	{
		if (state != newState) {
			Debug.Log("SetState: " + newState);
			state = newState;
			switch (state) {
			case States.WANDERING:
				PickWanderingPosition();
				break;
			}
		}
		lastTimeStateChanged = Time.time;
	}

	IEnumerator PickRotation()
	{
		while (true) {
			float angle = Random.Range(0.0F, 360.0f);
			rotation = Quaternion.Euler(0.0f, angle, 0.0f);
			yield return new WaitForSeconds(rotationChangeSeconds);
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player") {
			HitPlayer();
		}
	}

	void HitPlayer()
	{
		GameObject gameManagerObject = GameObject.FindGameObjectWithTag("GameManager");
		GameManager gameManager = gameManagerObject.GetComponent<GameManager>();
		gameManager.SubtractFromPlayerHealth(damage);
		Destroy(gameObject);
	}

	public bool SubtractFromHealth(int amount)
	{
		health -= amount;
		Debug.Log("ENEMY HEALTH: " + health.ToString());
		if (health <= 0)
		{
			Destroy(gameObject);
			return true;
		}
		return false;
	}

}
