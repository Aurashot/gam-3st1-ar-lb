﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour {

    public GameObject enemyPrefab;

	void Start()
	{
	}

	public void SpawnEnemies()
	{
		// find all spawn points
        GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag("Spawn");
        // spawn an enemy at the points
        foreach (GameObject spawnPoint in spawnPoints)
        {
        	Debug.Log("SPAWNING AN ENERMIE!");
            GameObject enemy = Instantiate(enemyPrefab);
            enemy.transform.position = spawnPoint.transform.position;
        }
	}

	public void StartSpawn(Vector3 planePosition, Quaternion planeRotation)
	{
		transform.position = planePosition;
		transform.rotation = planeRotation;
		SpawnEnemies();
	}
	
}
