﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class GameManager : MonoBehaviour
{

	public Text clueText;
	public Text winText;
	public List<GameObject> images;

	private List<GameObject> foundClues = new List<GameObject>();

	void Start()
	{
	}
	
	void Update()
	{
	}

	void UpdateClueText()
	{
		List<string> textStrings = new List<string>();
		foreach (GameObject clueObject in foundClues)
		{
			textStrings.Add(clueObject.name);
		}
		clueText.text = string.Join("\n", textStrings.ToArray());
	}

	public void FoundClue(GameObject clueObject)
	{
		if (!foundClues.Contains(clueObject))
		{
			foundClues.Add(clueObject);
			UpdateClueText();
			if (images.Count == foundClues.Count)
			{
				winText.text = "YOU WIN!";
			}
		}
	}

}
